package org.vergien.buildtest;

public class AppA {
	private LibA libA = new LibA();

	public String getGreeting() {
		String hello = libA.getHello();
		System.out.println(hello);
		return hello;
	}
}
