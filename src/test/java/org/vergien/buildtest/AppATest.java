package org.vergien.buildtest;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class AppATest {

	@Test
	public void testGetGreeting() {
		AppA appA = new AppA();

    assertThat(appA.getGreeting(), startsWith("Hello, World!"));
	}

}
